# vvv-utils

>>>
Update 31/01/2022: `php81` is already included in the official [vvv-utilities](https://github.com/Varying-Vagrant-Vagrants/vvv-utilities/commit/ad97e27f035baeb4f59cde2f06f650f68ec70297)
>>>

## How to use

  1. Add the repository to the `utility-sources` section of `config/config.yml` file in VVV.
```yaml
utility-sources:
  vvv-utils:
    repo: https://gitlab.com/aranofacundo/vvv-utils.git
    branch: main
```

  2. Then add the utilities you need from the repo to the `utilities` section `config/config.yml` file in VVV.
> Keep in mind that:
>  - The name you used to define the utility source must be the same in the utilities section, in this case `vvv-utils`.
>  - The name of the utilities must correspond to the folder name in the root folder, for example `php81`.

```yaml
utilities:
  core: # The core VVV utility
    - tls-ca # HTTPS SSL/TLS certificates
    - phpmyadmin # Web based database client
    #- memcached-admin # Object cache management
    #- opcache-status # opcache management
    #- webgrind # PHP Debugging
    #- mongodb # needed for Tideways/XHGui
    #- tideways # PHP profiling tool, also installs xhgui check https://varyingvagrantvagrants.org/docs/en-US/references/tideways-xhgui/
    #- nvm # Node Version Manager
    #- php56
    #- php70
    #- php71
    #- php72
    #- php73
    #- php74
    - php80
  vvv-utils:
    - php81
```

  3. Run VVV provision: `vagrant up --provision`

## Known Issues
  - To run a WordPress site with `php81`, you'll need to install the version 5.9.x or above. WordPress 5.8 won't work. _(Update 05/01/2022: There're still some warning with WP 5.9.0.rc1)_
